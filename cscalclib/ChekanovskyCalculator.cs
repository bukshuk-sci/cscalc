﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalclib
{
	class ChekanovskyCalculator : CalculatorBase
	{
		public ChekanovskyCalculator(Sample[] samples, Grade[] grades)
			: base(samples, grades)
		{
		}

		protected override decimal ComparisonFactor(Sample sampleOne, Sample sampleTwo)
		{
			decimal total = 0;
			decimal minSum = 0;

			for (uint i = 0; i < sampleOne.SpeciesQuantitiesNumber; i++)
			{
				total += sampleOne[i] + sampleTwo[i];
				minSum += Math.Min(sampleOne[i], sampleTwo[i]);
			}

			return 2.0m * minSum / total;
		}
	}
}
