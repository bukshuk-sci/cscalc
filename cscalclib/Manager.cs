﻿using baselib.Text;
using interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("cscalctest")]

namespace cscalclib
{
	public static class Manager
	{
		static Manager()
		{
			if (!Directory.Exists(Settings.Default.ToFolder))
			{
				Directory.CreateDirectory(Settings.Default.ToFolder);
			}
		}

		public static void GenerateReports()
		{
			ReadInputData();

			var estimator = new Estimator(Manager.Samples, Manager.GradesNames);
			estimator.Calculate();

			var shuffler = new Shuffler(Manager.Samples, Manager.GradesNames);
			shuffler.Shuffle();
			shuffler.Calculate(estimator.ChekanovskyCalc.M, estimator.SorensenCalc.M);

			ExportMatrixSheets(estimator);
			ExportReportSheet(estimator, shuffler);

			//TODO: Export shuffle.csv optionally
		}

		internal static void ReadInputData()
		{
			string[] lines = File.ReadAllLines(InputFilePath());

			Signaller.Exception(lines.Length > 1, "Must be multiple samples");

			Manager.Samples = new Sample[lines.Length];
			Manager.GradesNames = new string[lines.Length];

			int columnsNumber = 0;
			for (int i = 0; i < lines.Length; i++)
			{
				string[] columns = lines[i].Split(CultureInfo.CurrentCulture.TextInfo.ListSeparator[0]);

				if (i == 0)
				{
					Signaller.Exception(columns.Length > 2, "Must be more than two columns");
					columnsNumber = columns.Length;
				}
				else
				{
					Signaller.Exception(columns.Length == columnsNumber, "Must be equal columns number");
				}

				Manager.GradesNames[i] = columns[0];
				Manager.Samples[i] = new Sample(columns.Skip(1).ToArray());
			}
		}

		
		private static void ExportMatrixSheets(Estimator estimator)
		{
			ExportSheet(OutputFilePath("sorcomp.csv"), estimator.SorensenCalc.ComparisonMatrix);
			ExportSheet(OutputFilePath("checomp.csv"), estimator.ChekanovskyCalc.ComparisonMatrix);
			ExportSheet(OutputFilePath("sormean.csv"), estimator.SorensenCalc.MeanValuesMatrix);
			ExportSheet(OutputFilePath("chemean.csv"), estimator.ChekanovskyCalc.MeanValuesMatrix);
		}

		private static void ExportReportSheet(Estimator estimator, Shuffler shuffler)
		{
			CsvWriter cw = new CsvWriter();

			cw.AddRow("Method", "b", "w", "M", "M >", "M =", "M <");

			//TODO: Check that I do not mess up two algorithms
			cw.AddRow("Sorensen", NumberToString(estimator.SorensenCalc.BMean),
				NumberToString(estimator.SorensenCalc.WMean),
				NumberToString(estimator.SorensenCalc.M),
				shuffler.SorensenLessMore.Item1.ToString(),
				shuffler.SorensenLessMore.Item2.ToString(),
				shuffler.SorensenLessMore.Item3.ToString());

			cw.AddRow("Chekanovsky", NumberToString(estimator.ChekanovskyCalc.BMean),
				NumberToString(estimator.ChekanovskyCalc.WMean),
				NumberToString(estimator.ChekanovskyCalc.M),
				shuffler.ChekanovskyLessMore.Item1.ToString(),
				shuffler.ChekanovskyLessMore.Item2.ToString(),
				shuffler.ChekanovskyLessMore.Item3.ToString());

			cw.Save(OutputFilePath("report.csv"));
		}

		private static void ExportSheet(string outputFile, decimal[,] matrix)
		{
			CsvWriter cw = new CsvWriter();

			int size = matrix.GetLength(0);
			for (int i = 0; i < size; i++)
			{
				string[] row = new string[size];

				for (int j = 0; j < size; j++)
				{
					if (matrix[i, j] != CalculatorBase.EmptyMarker)
					{
						row[j] = NumberToString(matrix[i, j]);
					}
					else
					{
						row[j] = string.Empty;
					}
				}

				cw.AddRow(row);
			}

			cw.Save(outputFile);
		}


		private static string NumberToString(decimal number)
		{
			int NumberOfFrctionalDigits = Settings.Default.NumberOfFrctionalDigits;

			return Math.Round(number, NumberOfFrctionalDigits).ToString();
		}

		private static string InputFilePath()
		{
			string inputFilePath = Settings.Default.InputFilePath;

			if (!File.Exists(inputFilePath))
			{
				Signaller.Exception("Cannot find INPUT file: {0}", inputFilePath);
			}

			return inputFilePath;
		}

		private static string OutputFilePath(string fileName)
		{
			if (!Directory.Exists(Settings.Default.ToFolder))
			{
				Signaller.Exception("Cannot find OUTPUT directory: {0}", Settings.Default.ToFolder);
			}

			return Path.Combine(Settings.Default.ToFolder, fileName);
		}

		
		internal static Sample[] Samples
		{
			get;
			private set;
		}

		internal static string[] GradesNames
		{
			get;
			private set;
		}

		
		private static uint HigherThanChekanovsky { get; set; }

		private static uint HigherThanSorensen { get; set; }

		private static int LowerThanChekanovsky { get; set; }

		private static int LowerThanSorensen { get; set; }
	}
}
