Программа предназначена для оценки надежности распределения по классам (classification strength)
биологических проб по индексам видового сходства Съёренсена (Sőrensen, 1948) и Чекановского (Czekanowcki, 1911).
Алгоритм предполагает попарное сравнение всех проб в массиве и расчет индексов видового сходства для каждой пары.
Далее рассчитываются и сравниваются средние значения инексов при внутриклассовых и межклассовых сравнениях.

The program is designed to assess the reliability of distribution by class (classification)
of biological samples according to the indices of species similarity of Sörensen (Sőrensen, 1948) and Chekanovsky (Czekanowcki, 1911).
The algorithm assumes a pair-wise comparison of all samples in the array and the calculation of indices of species similarity for each pair.
Next, the average values of the inices are calculated and compared with intraclass and interclass comparisons.