﻿using cscalclib;
using interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace cscalc
{
	class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				Manager.GenerateReports();
			}
			catch (Exception ex)
			{
				Signaller.Assertion(ex);
			}

			Console.WriteLine();
			Console.WriteLine("Press Return to continue ...");
			Console.ReadLine();
		}
	}
}
