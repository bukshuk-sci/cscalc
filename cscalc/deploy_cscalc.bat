@ECHO OFF

IF "%BKP_VS%" == "" GOTO NOBKPVS

SET source=%BKP_VS%\Projects\vcp_cs\cscalc\cscalc\bin\Release\
SET target=%USERPROFILE%\Desktop\cscalc

IF NOT EXIST %target% MKDIR %target%

xcopy /Y "%source%cscalc.exe" "%target%"
xcopy /Y "%source%cscalc.exe.config" "%target%"
xcopy /Y "%source%cscalclib.dll" "%target%"
xcopy /Y "%source%base.dll" "%target%"
xcopy /Y "%source%interfaces.dll" "%target%"


GOTO END

:NOBKPVS
ECHO The BKP_VS environment variable  is not set

:END
IF "%1"=="" (
  TIMEOUT /T 10
)
